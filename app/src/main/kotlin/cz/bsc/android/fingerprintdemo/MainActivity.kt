package cz.bsc.android.fingerprintdemo

import android.app.ProgressDialog
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat
import android.support.v4.os.CancellationSignal
import android.support.v7.app.AppCompatActivity
import android.util.Base64
import android.util.Base64.NO_PADDING
import cz.bsc.android.fingerprint.util.FingerprintHelper
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.*
import java.nio.charset.Charset
import javax.crypto.Cipher

class MainActivity : AppCompatActivity() {

    lateinit var fingerprintHelper: FingerprintHelper
    lateinit var fingerprintManager: FingerprintManagerCompat
    lateinit var prefs: SharedPreferences


    private val KEY_ALIAS = "FingerprintDemo"

    open inner class MyAuthCallback(val onAuth: () -> Unit, val dialog: ProgressDialog) : FingerprintManagerCompat.AuthenticationCallback() {
        override fun onAuthenticationSucceeded(result: FingerprintManagerCompat.AuthenticationResult?) {
            dialog.dismiss()
            onAuth()
        }

        override fun onAuthenticationError(errMsgId: Int, errString: CharSequence?) {
            super.onAuthenticationError(errMsgId, errString)
            dialog.dismiss()
            errorDialog(errString?.toString())
        }

        override fun onAuthenticationHelp(helpMsgId: Int, helpString: CharSequence?) {
            super.onAuthenticationHelp(helpMsgId, helpString)
            dialog.dismiss()
            errorDialog(helpString?.toString())
        }

        override fun onAuthenticationFailed() {
            super.onAuthenticationFailed()
            dialog.dismiss()
            errorDialog("Authentication failed.")
        }
    }


    private val ENCRYPTED_PASSCODE = "encryptedPasscode"

    private val IV = "iv"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        prefs = PreferenceManager.getDefaultSharedPreferences(this)
        edEncrypted.setText(prefs.getString(ENCRYPTED_PASSCODE, ""))
        edIv.setText(prefs.getString(IV, ""))
        val UTF8=Charset.forName("UTF-8")
        if (FingerprintHelper.isFingerprintSupported(this)) {
            fingerprintHelper = FingerprintHelper.from(applicationContext, KEY_ALIAS)
            val dialog = progressDialog(R.string.initializing) {
                setProgressStyle(ProgressDialog.STYLE_SPINNER)
                setCancelable(false)
            }
            async() {
                fingerprintHelper.createKey()
                activityUiThread { dialog.dismiss() }
            }
            fingerprintManager = FingerprintManagerCompat.from(this)

            btEncrypt.onClick {
                fingerprintHelper.initCipher(Cipher.ENCRYPT_MODE, null)
                edIv.setText(Base64.encodeToString(fingerprintHelper.cipher.iv, NO_PADDING))
                val cancellationSignal = CancellationSignal()
                val dialog2 = progressDialog(R.string.useFingerToAuth) {
                    setProgressStyle(ProgressDialog.STYLE_SPINNER)
                    setOnCancelListener { cancellationSignal.cancel() }
                }
                val authenticationCallback = MyAuthCallback ({
                    val passcode = edPasscode.text.toString().toByteArray()
                    val encrypted = fingerprintHelper.encrypt(passcode)
                    edEncrypted.setText(Base64.encodeToString(encrypted, NO_PADDING))
                }, dialog2)
                val cryptoObject = FingerprintManagerCompat.CryptoObject(fingerprintHelper.cipher)
                fingerprintManager.authenticate(cryptoObject, 0, cancellationSignal, authenticationCallback, null)

            }

            btDecrypt.onClick {
                val iv = Base64.decode(edIv.text?.toString(), NO_PADDING)
                fingerprintHelper.initCipher(Cipher.DECRYPT_MODE, iv)
                val cancellationSignal = CancellationSignal()
                val dialog2 = progressDialog(R.string.useFingerToAuth) {
                    setProgressStyle(ProgressDialog.STYLE_SPINNER)
                    setOnCancelListener { cancellationSignal.cancel() }
                }
                val authenticationCallback = object: MyAuthCallback ({
                    val encrypted = Base64.decode(edEncrypted.text?.toString(), NO_PADDING)
                    val decrypted = fingerprintHelper.decrypt(encrypted)
                    edDecryptedPasscode.setText(decrypted.toString(UTF8))
                }, dialog2) {
                    override fun onAuthenticationFailed() {
                        super.onAuthenticationFailed()
                        cancellationSignal.cancel()
                    }
                }
                val cryptoObject = FingerprintManagerCompat.CryptoObject(fingerprintHelper.cipher)
                fingerprintManager.authenticate(cryptoObject, 0, cancellationSignal, authenticationCallback, null)
            }
        } else {
            alert(R.string.fingerprintNA) {
                positiveButton(R.string.ok) {
                    dismiss()
                    finish()
                }
                onCancel { finish() }
            }.show()
        }
    }

    private fun errorDialog(msg: String?) {
        alert(msg ?: "") {
            positiveButton(R.string.ok) {
                dismiss()
            }
        }.show()
    }

    override fun onPause() {
        super.onPause()
        val editor = prefs.edit()
        editor.putString(ENCRYPTED_PASSCODE, edEncrypted.text.toString())
        editor.putString(IV, edIv.text.toString())
        editor.apply()
    }
}
