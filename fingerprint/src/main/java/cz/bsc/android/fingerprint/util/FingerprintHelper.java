package cz.bsc.android.fingerprint.util;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat;
import android.util.Log;
import lombok.Getter;
import lombok.val;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;

/**
 * Utility methods for fingerprint handling.
 */
public class FingerprintHelper {

    private static final String TAG = "FingerprintHelper";
    private Context mContext;
    private KeyGenerator mKeyGenerator;
    private KeyStore mKeyStore;
    private String mKeyAlias;
    @Getter
    private Cipher cipher;
    private Signature mSignature;

    private FingerprintHelper() {
        //not to be constructed
    }

    public static FingerprintHelper from(Context appContext, String keyAlias) throws KeyStoreException {
        if (!(appContext instanceof Application)) {
            throw new IllegalArgumentException("Please use Application context here");
        } else {
            if (isFingerprintSupported(appContext)) {
                try {
                    val instance = new FingerprintHelper();
                    instance.mContext = appContext;
                    instance.mKeyGenerator = getKeyGenerator();
                    instance.mKeyStore = getKeyStore();
                    instance.mKeyStore.load(null);
                    instance.mKeyAlias = keyAlias;
                    instance.cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7);
                    return instance;
                } catch (Exception e) {
                    throw new FingerprintException("Keystore initialization failed.", e);
                }
            } else {
                throw new FingerprintException("Fingerprint not supported.", null);
            }
        }
    }

    public static boolean isFingerprintSupported(Context ctx) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                getKeyStore();
                getKeyGenerator();
                return (FingerprintManagerCompat.from(ctx).isHardwareDetected());
            } else {
                return false;
            }
        } catch (Exception e) {
            Log.e(TAG, "initialization error", e);
            return false;
        }
    }

    public static KeyStore getKeyStore() throws KeyStoreException {
        return KeyStore.getInstance("AndroidKeyStore");

    }

    public static KeyGenerator getKeyGenerator() throws KeyStoreException {
        try {
            return KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
        } catch (NoSuchAlgorithmException | NoSuchProviderException exception) {
            throw new KeyStoreException(exception);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void createKey() {
        try {
            if (!mKeyStore.containsAlias(mKeyAlias)) {
                mKeyGenerator.init(new KeyGenParameterSpec.Builder(mKeyAlias, KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT).setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                        .setUserAuthenticationRequired(true).setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7).build());
                mKeyGenerator.generateKey();
            } else {
                Log.d(TAG, "Key is already present.");
            }
        } catch (KeyStoreException | InvalidAlgorithmParameterException exception) {
            throw new FingerprintException("Failed to generate key pair", exception);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean initCipher(int opmode, byte[] iv) throws FingerprintException {
        try {
            mKeyStore.load(null);
            SecretKey key = (SecretKey) mKeyStore.getKey(mKeyAlias, null);
            cipher.init(opmode, key, iv != null ? new IvParameterSpec(iv) : null);
            return true;
        } catch (KeyPermanentlyInvalidatedException exception) {
            return false;
        } catch (InvalidAlgorithmParameterException | KeyStoreException | IOException | NoSuchAlgorithmException | UnrecoverableEntryException | InvalidKeyException | CertificateException exception) {
            throw new FingerprintException("cipher initialization failed", exception);
        }
    }

    public byte[] encrypt(byte[] password) {
        try {
            return cipher.doFinal(password);
        } catch (Exception exception) {
            throw new RuntimeException("Failed to encrypt password", exception);
        }
    }

    public byte[] decrypt(byte[] encrypted) {
        try {
            return cipher.doFinal(encrypted);
        } catch (IllegalBlockSizeException | BadPaddingException exception) {
            throw new RuntimeException("Failed to decrypt password", exception);
        }
    }

    public static class FingerprintException extends RuntimeException {

        public FingerprintException(String s, Throwable throwable) {
            super(s, throwable);
        }
    }
}
