package cz.bsc.android.fingerprint.util;

import javax.crypto.Mac;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * @author sarossy
 * @version $Revision: 326282 $
 */
public class PBKDF2 {

    private static final String TAG = PBKDF2.class.getName();

    // ctor
    private PBKDF2() {
    }

    public static byte[] deriveKey(byte[] password, byte[] salt, String digestFuncName, int iterationCount, int dkLen) {
        try {
            SecretKeySpec keySpec = new SecretKeySpec(password, digestFuncName);
            Mac prf = Mac.getInstance(keySpec.getAlgorithm());
            prf.init(keySpec);

            // Note: hLen, dkLen, l, r, T, F, etc. are horrible names for
            // variables and functions in this day and age, but they
            // reflect the terse symbols used in RFC 2898 to describe
            // the PBKDF2 algorithm, which improves validation of the
            // code vs. the RFC.
            //
            // dkLen is expressed in bytes. (32 for a 256-bit key)

            int hLen = prf.getMacLength(); // 32 for SHA-256
            int l = (dkLen - 1) / hLen + 1; // 1 for 256bit keys with SHA-256
            byte T[] = new byte[dkLen];
            int tOffset = 0;
            for (int i = 1; i <= l; i++) {
                F(T, tOffset, prf, salt, iterationCount, i);
                tOffset += hLen;
            }

            return T;
        } catch (ShortBufferException | NoSuchAlgorithmException | InvalidKeyException | IllegalStateException e) {
            throw new RuntimeException(e);
        }
    }

    public static byte[] deriveKey(String password, byte[] salt, String digestFuncName, int iterationCount, int dkLen) {
        try {
            return deriveKey(password.getBytes("UTF-8"), salt, digestFuncName, iterationCount, dkLen);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private static void F(byte[] dest, int destOffset, Mac prf, byte[] S, int c, int blockIndex) throws ShortBufferException, IllegalStateException {
        // U0 = S || INT (i);
        prf.update(S);
        prf.update((byte) (blockIndex >> 24));
        prf.update((byte) (blockIndex >> 16));
        prf.update((byte) (blockIndex >> 8));
        prf.update((byte) blockIndex);

        byte Ui[] = prf.doFinal();
        System.arraycopy(Ui, 0, dest, destOffset, Math.min(Ui.length, dest.length - destOffset));

        for (int i = 1; i < c; i++) {
            prf.update(Ui);
            prf.doFinal(Ui, 0);
            xor(dest, destOffset, Ui);
        }
    }

    private static void xor(byte[] dest, int destOffset, byte[] src) {
        int length = Math.min(src.length, dest.length - destOffset);
        for (int i = 0; i < length; i++) {
            dest[destOffset + i] ^= src[i];
        }
    }
}
