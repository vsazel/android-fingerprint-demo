package cz.bsc.android.fingerprint.util;

import android.util.Base64;
import lombok.Builder;
import lombok.Getter;

import javax.crypto.Cipher;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.X509EncodedKeySpec;
import java.util.Random;

/**
 * Helper for handling passcode.
 * <p/>
 * <code>
 * <p/>
 * </code>
 */
@Builder
public class PasscodeHelper {

    private static final String RSA_NONE_PKCS1_PADDING = "RSA/NONE/PKCS1Padding";
    private static final String BC = "BC";
    private static final String RSA = "RSA";
    @Getter
    private byte[] publicKey = null;

    @Getter
    private int passCodeSize = 5;

    @Getter
    private byte[] passcode = null;

    @Getter
    private byte[] registrationId = null;

    @Getter
    public String digestFunctionName = "HmacSHA256";

    @Getter
    public int iterationCount = 4096;

    @Getter
    public boolean randomPasscode;

    @Getter
    public int derivedKeyLength = 32;

    public static class PasscodeException extends Exception {

        public PasscodeException(String detailMessage) {
            super(detailMessage);
        }
    }

    /**
     * Encrypts passcode for sending to server during registration with server's public key.
     *
     * @return computed passcode bytes
     */
    public byte[] computeSecurePasscode() throws PasscodeException {
        final Random r = new SecureRandom();
        if (randomPasscode) {
            passcode = new byte[passCodeSize];
            r.nextBytes(passcode);
        }
        if (passcode == null) {
            byte[] salt = new byte[240];
            r.nextBytes(salt);

            byte[] saltedPasscode = new byte[passcode.length + salt.length];
            System.arraycopy(passcode, 0, saltedPasscode, 0, passcode.length);
            System.arraycopy(salt, 0, saltedPasscode, passcode.length, salt.length);

            Cipher cipher = null;
            try {
                cipher = Cipher.getInstance(RSA_NONE_PKCS1_PADDING, BC);
                X509EncodedKeySpec key = new X509EncodedKeySpec(publicKey);
                PublicKey publicKey = KeyFactory.getInstance(RSA, BC).generatePublic(key);
                cipher.init(Cipher.ENCRYPT_MODE, publicKey);
                return cipher.doFinal(saltedPasscode);
            } catch (Exception e) {
                throw new PasscodeException("Encryption of passcode failed, during computeSecurePassword()");
            }
        } else {
            throw new PasscodeException("Passcode not set.");
        }
    }

    /**
     * Encrypts passcode for sending to server during registration with server's public key.
     *
     * @return computed passcode Base64 encoded
     */
    public String computeSecurePasscodeB64() throws PasscodeException {
        return Base64.encodeToString(computeSecurePasscode(), Base64.NO_WRAP);
    }

    /**
     * Encrypts passcode using public key for sending during passcode login.
     *
     * @param saltBytes salt bytes
     *
     * @return salted passcode encrypted by public key in bytes
     */
    public byte[] computeSaltedPasscode(byte[] saltBytes) throws PasscodeException {

        byte[] computedKey = PBKDF2.deriveKey(passcode, saltBytes, digestFunctionName, iterationCount, derivedKeyLength);

        final Random r = new SecureRandom();
        byte[] salt = new byte[213];
        r.nextBytes(salt);
        if (passcode != null) {
            byte[] saltedPasscode = new byte[computedKey.length + salt.length];
            System.arraycopy(computedKey, 0, saltedPasscode, 0, computedKey.length);
            System.arraycopy(salt, 0, saltedPasscode, computedKey.length, salt.length);
            Cipher cipher;
            try {
                cipher = Cipher.getInstance(RSA_NONE_PKCS1_PADDING, BC);
                PublicKey publicKey = KeyFactory.getInstance(RSA).generatePublic(new X509EncodedKeySpec(this.publicKey));
                cipher.init(Cipher.ENCRYPT_MODE, publicKey);
                return cipher.doFinal(saltedPasscode);
            } catch (Exception e) {
                throw new PasscodeException("Encryption of passcode failed, during computeSaltedPasscode()");
            }
        } else {
            throw new PasscodeException("Passcode not set.");
        }
    }

    /**
     * Encrypts passcode using public key for sending during passcode login.
     *
     * @param saltBytes salt bytes
     *
     * @return salted passcode encrypted by public key in Base64 encoded string
     */
    public String computeSaltedPasscodeB64(byte[] saltBytes) throws PasscodeException {
        return Base64.encodeToString(saltBytes, Base64.NO_WRAP);
    }

}
